﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebTesis.Models;

namespace WebTesis.Controllers
{
    public class ticketsController : Controller
    {
        private tesisEntities db = new tesisEntities();

        // GET: tickets
        public ActionResult Index()
        {
            var tickets = db.tickets.Include(t => t.categorias).Include(t => t.estados).Include(t => t.usuarios);
            return View(tickets.ToList());
        }

        // GET: tickets/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tickets tickets = db.tickets.Find(id);
            if (tickets == null)
            {
                return HttpNotFound();
            }
            return View(tickets);
        }

        // GET: tickets/Create
        public ActionResult Create()
        {
            ViewBag.id_categoria = new SelectList(db.categorias, "id_categoria", "categoria");
            ViewBag.id_estado = new SelectList(db.estados, "id_estado", "estado");
            ViewBag.id_usuario = new SelectList(db.usuarios, "id_usuario", "nombre");
            return View();
        }

        // POST: tickets/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_ticket,id_usuario,id_categoria,problema,telefono,correo,id_estado,fecha")] tickets tickets)
        {
            if (ModelState.IsValid)
            {
                db.tickets.Add(tickets);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_categoria = new SelectList(db.categorias, "id_categoria", "categoria", tickets.id_categoria);
            ViewBag.id_estado = new SelectList(db.estados, "id_estado", "estado", tickets.id_estado);
            ViewBag.id_usuario = new SelectList(db.usuarios, "id_usuario", "nombre", tickets.id_usuario);
            return View(tickets);
        }

        // GET: tickets/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tickets tickets = db.tickets.Find(id);
            if (tickets == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_categoria = new SelectList(db.categorias, "id_categoria", "categoria", tickets.id_categoria);
            ViewBag.id_estado = new SelectList(db.estados, "id_estado", "estado", tickets.id_estado);
            ViewBag.id_usuario = new SelectList(db.usuarios, "id_usuario", "nombre", tickets.id_usuario);
            return View(tickets);
        }

        // POST: tickets/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_ticket,id_usuario,id_categoria,problema,telefono,correo,id_estado,fecha")] tickets tickets)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tickets).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_categoria = new SelectList(db.categorias, "id_categoria", "categoria", tickets.id_categoria);
            ViewBag.id_estado = new SelectList(db.estados, "id_estado", "estado", tickets.id_estado);
            ViewBag.id_usuario = new SelectList(db.usuarios, "id_usuario", "nombre", tickets.id_usuario);
            return View(tickets);
        }

        // GET: tickets/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tickets tickets = db.tickets.Find(id);
            if (tickets == null)
            {
                return HttpNotFound();
            }
            return View(tickets);
        }

        // POST: tickets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tickets tickets = db.tickets.Find(id);
            db.tickets.Remove(tickets);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
