﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebTesis.Models;

namespace WebTesis.Controllers
{
    public class LoginsController : Controller
    {
        // GET: Logins
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AutorizarLogin(WebTesis.Models.usuarios users)
        {
            using (tesisEntities entity = new tesisEntities())
            {
                var loginusuario = entity.usuarios.Where(x => x.usuario == users.usuario && x.pass == users.pass).FirstOrDefault();

                if (loginusuario == null || loginusuario.id_tipo_usuario == 3)
                {
                    users.LoginError = "Usuario o Clave invalida";
                    return View("Login", users);
                }
                else
                {
                    Session["tipouser"] = loginusuario.id_tipo_usuario;
                    return RedirectToAction("Index", "Home");
                }
            }
        }

        [HttpPost]
        public ActionResult VerificarTipoUser(WebTesis.Models.usuarios user)
        {
            using (tesisEntities entity = new tesisEntities())
            {
                var tipouser = entity.usuarios.Where(x => x.id_tipo_usuario.Equals(1) || x.id_tipo_usuario.Equals(4)).FirstOrDefault();

                if (tipouser == null)
                {
                    return RedirectToAction("Shared", "Error");
                }
                else
                {
                    return RedirectToAction("Index", "usuarios");
                }
            }

        }

        public ActionResult LogOut()
        {
            Session.Abandon();
            return RedirectToAction("Login", "Logins");
        }

    }
}