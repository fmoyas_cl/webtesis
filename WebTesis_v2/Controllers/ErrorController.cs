﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebTesis.Controllers
{
    public class ErrorController : Controller
    {
        // GET: Error
        public ActionResult Index()
        {
            string tipouser = Session["tipouser"].ToString();

            if(tipouser.Equals(4))
            {
                return RedirectToAction("Index", "usuarios");
            }
            else
            {
                return View();
            }
           
        }
    }
}