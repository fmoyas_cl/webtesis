﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebTesis.Models;

namespace WebTesis.Controllers
{
    public class aprobacionController : Controller
    {
        private tesisEntities db = new tesisEntities();

        // GET: aprobacion
        public ActionResult Index()
        {

            var aprobacion = db.aprobacion.Include(a => a.categorias).Include(a => a.usuarios);
            return View(aprobacion.ToList());

        }

        //Consultas SQL para contar aprobacion de la aplicacion
        public ActionResult HardwareSI()
        {
            return Content(db.aprobacion.Where(a => a.aprueba.Equals("SI") && a.categoria.Equals(1)).Count().ToString());
        }

        public ActionResult HardwareNO()
        {
            return Content(db.aprobacion.Where(a => a.aprueba.Equals("NO") && a.categoria.Equals(1)).Count().ToString());
        }

        public ActionResult SoftwareSI()
        {
            return Content(db.aprobacion.Where(a => a.aprueba.Equals("SI") && a.categoria.Equals(2)).Count().ToString());
        }

        public ActionResult SoftwareNO()
        {
            return Content(db.aprobacion.Where(a => a.aprueba.Equals("NO") && a.categoria.Equals(2)).Count().ToString());
        }

        //select count(aprueba) from aprobacion where aprueba = 'SI' and categoria = 1 //hardware ***

        //select count(aprueba) from aprobacion where aprueba = 'NO' and categoria = 1 //hardware

        //select count(aprueba) from aprobacion where aprueba = 'SI' and categoria = 2 //software

        //select count(aprueba) from aprobacion where aprueba = 'NO' and categoria = 2 //software


        // GET: aprobacion/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            aprobacion aprobacion = db.aprobacion.Find(id);
            if (aprobacion == null)
            {
                return HttpNotFound();
            }
            return View(aprobacion);
        }

        // GET: aprobacion/Create
        public ActionResult Create()
        {
            ViewBag.categoria = new SelectList(db.categorias, "id_categoria", "categoria");
            ViewBag.id_usuario = new SelectList(db.usuarios, "id_usuario", "nombre");
            return View();
        }

        // POST: aprobacion/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_aprobacion,id_usuario,categoria,aprueba,fecha")] aprobacion aprobacion)
        {
            if (ModelState.IsValid)
            {
                db.aprobacion.Add(aprobacion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.categoria = new SelectList(db.categorias, "id_categoria", "categoria", aprobacion.categoria);
            ViewBag.id_usuario = new SelectList(db.usuarios, "id_usuario", "nombre", aprobacion.id_usuario);
            return View(aprobacion);
        }

        // GET: aprobacion/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            aprobacion aprobacion = db.aprobacion.Find(id);
            if (aprobacion == null)
            {
                return HttpNotFound();
            }
            ViewBag.categoria = new SelectList(db.categorias, "id_categoria", "categoria", aprobacion.categoria);
            ViewBag.id_usuario = new SelectList(db.usuarios, "id_usuario", "nombre", aprobacion.id_usuario);
            return View(aprobacion);
        }

        // POST: aprobacion/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_aprobacion,id_usuario,categoria,aprueba,fecha")] aprobacion aprobacion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(aprobacion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.categoria = new SelectList(db.categorias, "id_categoria", "categoria", aprobacion.categoria);
            ViewBag.id_usuario = new SelectList(db.usuarios, "id_usuario", "nombre", aprobacion.id_usuario);
            return View(aprobacion);
        }

        // GET: aprobacion/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            aprobacion aprobacion = db.aprobacion.Find(id);
            if (aprobacion == null)
            {
                return HttpNotFound();
            }
            return View(aprobacion);
        }

        // POST: aprobacion/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            aprobacion aprobacion = db.aprobacion.Find(id);
            db.aprobacion.Remove(aprobacion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
